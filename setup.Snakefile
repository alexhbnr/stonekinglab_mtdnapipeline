################################################################################
# Snakefile for preparing all the resources before being able to run the
# mitoBench ancient DNA MT pipeline.
#
# Alex Huebner, 08/01/2020
################################################################################

workdir: "/tmp"

# Constants
PATH = workflow.basedir
HUMAN_MT_FAS = f"{PATH}/resources/NC_012920.fa.gz"

localrules: decompress_fasta, bwa_index, samtools_index

rule all:
    input: 
        f"{PATH}/resources/NC_012920_1000.fa.ann",
        f"{PATH}/resources/NC_012920.fa.fai",
        f"{PATH}/resources/haplogrep-2.1.25.jar"

# Prepare MT genome for analysis

rule decompress_fasta:
    output:
        "{PATH}/resources/NC_012920.fa"
    message: "De-compress the FastA sequence of the human MT genome with 1000 bp extension"
    conda: f"{PATH}/envs/setup.yaml"
    shell:
        "gunzip -c {HUMAN_MT_FAS} > {output}"

rule extend_fasta:
    input:
        "{PATH}/resources/NC_012920.fa"
    output:
        "{PATH}/resources/NC_012920_1000.fa"
    conda: f"{PATH}/envs/setup.yaml"
    shell:
        """
        bioawk -c fastx '{{
            print ">" $name; \
            print $seq substr($seq,1,1000); 
        }}' {input} > {output}
        """

rule bwa_index:
    input:
        "{PATH}/resources/NC_012920_1000.fa"
    output:
        "{PATH}/resources/NC_012920_1000.fa.ann"
    message: "BWA index the FastA sequence of the human MT genome with 1000 bp extension"
    conda: f"{PATH}/envs/setup.yaml"
    shell:
        "bwa index {input}"

rule samtools_index:
    input:
        "{PATH}/resources/NC_012920.fa"
    output:
        "{PATH}/resources/NC_012920.fa.fai"
    message: "Samtools index the FastA sequence of the human MT genome"
    conda: f"{PATH}/envs/setup.yaml"
    shell:
        "samtools faidx {input}"

rule fasta_dict:
    input:
        "{PATH}/resources/NC_012920.fa"
    output:
        "{PATH}/resources/NC_012920.dict"
    message: "Create sequence dictionary for the FastA sequence of the human MT genome"
    conda: f"{PATH}/envs/setup.yaml"
    shell:
        """
        picard CreateSequenceDictionary \
            R={input} \
            O={output}
        """


rule download_haplogrep:
    output:
        "{PATH}/resources/haplogrep-2.1.25.jar"
    message: "Download haplogrep-cmd from GitHub"
    params: url = "https://github.com/seppinho/haplogrep-cmd/releases/download/2.1.25/haplogrep-2.1.25.jar"
    shell:
        "wget -O {output} {params.url}"
