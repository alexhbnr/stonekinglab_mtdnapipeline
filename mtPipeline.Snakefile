################################################################################
# Snakefile for processing mtDNA-capture data of modern samples
#
# Alex Huebner, 08/01/2020
################################################################################

from snakemake.utils import min_version, R

min_version("5.0")
shell.executable("/bin/bash")

workdir: config['tmpdir']

# Create directory for cluster file if it doesn't exist

if not os.path.isdir(f"{config['tmpdir']}/cluster_logs"):
    os.makedirs(f"{config['tmpdir']}/cluster_logs")

# Infer expected sample ids and paths to the corresponding BAM files from config 

SAMPLES = [line.rstrip() for line in open(config['samplelist'], "rt")]
BAMS = {sample: "{}/{}.{}".format(config['bamdir'], sample, config['bamsuffix'])
        for sample in SAMPLES}

# Extract project directory
PROJDIR = config['projdir']

# Auxilliary functions
def mixemt_downsampling(flagstatfn, n=30000):
    ''' Determines the number of reads in a BAM file based on samtools flagstat
        and calculates the fraction of reads necessary to obtain 40,000 reads,
        the suggested input into mixEMT. The fraction will be used as input into
        samtools view -s for subsampling with SEED 0.
    '''
    if os.path.isfile(flagstatfn):
        with open(flagstatfn, "rt") as flagstatfile:
            nreads = int(next(flagstatfile).split(" ")[0])
        return "{:.4f}".format(n / nreads)
    else:
        return 1.0

localrules: copy_to_projdir, summary, remove_temp

rule all:
    input: 
        expand("{projdir}/{sample}/{sample}.vcf.gz", projdir=[PROJDIR], sample=SAMPLES),
        expand("{projdir}/sample_summary.csv", projdir=[PROJDIR]),
        expand("{projdir}/sequences.fasta", projdir=[PROJDIR])

rule extract_MTreads_and_filter:
    # Extract MT reads and known NUMTs and filter for QC fail
    output:
        temp("bam/{sample}.MT_numts.bam")
    message: "Filtering of reads (MT, Numts): {wildcards.sample}"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    params:
        numtbed = f"{workflow.basedir}/resources/nuMT_grch37.bed.gz",
        bam = lambda wildcards: BAMS[wildcards.sample]
    shell:
        """
        samtools view -bh -F 512 \
            -L {params.numtbed} \
            {params.bam} | \
        samtools sort -o {output} -
        """

rule convert_to_fastq:
    # Remove widow reads and convert to FastQ
    input:
        "bam/{sample}.MT_numts.bam"
    output:
        pe0 = temp("bam/{sample}.0.fq.gz"),
        pe1 = temp("bam/{sample}.1.fq.gz"),
        pe2 = temp("bam/{sample}.2.fq.gz")
    message: "Convert BAM to FastQ: {wildcards.sample}"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    params:
        bamfixpair = f"{workflow.basedir}/resources/bam-fixpair"
    shell:
        """
        {params.bamfixpair} --kill-widows -q {input} | \
        samtools fastq \
            -0 {output.pe0} \
            -1 {output.pe1} \
            -2 {output.pe2} -
        """

rule align_BWAmem_pe:
    input:
        pe0 = "bam/{sample}.0.fq.gz",
        pe1 = "bam/{sample}.1.fq.gz",
        pe2 = "bam/{sample}.2.fq.gz"
    output:
        temp("bam/{sample}.MT_pe.bam")
    message: "Align the paired reads against the MT using BWA mem: {wildcards.sample}"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    params:
        reffa = f"{workflow.basedir}/resources/NC_012920_1000.fa",
        bwaParams = "-B 6 -O 3 -E 2"
    threads: 4
    shell:
        """
        bwa mem -t {threads} {params.bwaParams} {params.reffa} \
            {input.pe1} {input.pe2} | \
        samtools view -Su - > {output}
        """

rule align_BWAmem_se:
    input:
        pe0 = "bam/{sample}.0.fq.gz",
        pe1 = "bam/{sample}.1.fq.gz",
        pe2 = "bam/{sample}.2.fq.gz"
    output:
        temp("bam/{sample}.MT_se.bam")
    message: "Align the single reads against the MT using BWA mem: {wildcards.sample}"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    params:
        reffa = f"{workflow.basedir}/resources/NC_012920_1000.fa",
        bwaParams = "-B 6 -O 3 -E 2"
    threads: 4
    shell:
        """
        bwa mem -t {threads} {params.bwaParams} {params.reffa} \
            {input.pe0} | \
        samtools view -Su - > {output}
        """

rule concat_addrg_rewrap_sort:
    input:
        pe = "bam/{sample}.MT_pe.bam",
        se = "bam/{sample}.MT_se.bam",
    output:
        temp("bam/{sample}.MTonly_sorted.bam")
    message: "Concatenate the paired and single reads, add the read group, re-wrap the overlap of the MT genome, and sort: {wildcards.sample}"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    params:
        bamrewrap = f"{workflow.basedir}/resources/bam-rewrap",
        rg = lambda wildcards: f"-r ID:{wildcards.sample} -r SM:{wildcards.sample}"
    shell:
        """
        samtools cat {input} | \
        samtools addreplacerg {params.rg} - | \
        samtools view -bh - | \
        {params.bamrewrap} MT:16569 | \
        samtools sort -o {output} -
        """

rule remove_duplicates:
    # Remove duplicated sequences
    input:
        "bam/{sample}.MTonly_sorted.bam"
    output:
        bam = temp("bam/{sample}.MTonly_sorted_rmdup.bam"),
        bai = temp("bam/{sample}.MTonly_sorted_rmdup.bam.bai"),
    message: "Remove duplicates: {wildcards.sample}"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    log: "log/markduplicates/{sample}.markduplicates.log"
    shell:
        """
        samtools sort -n {input} | \
        samtools fixmate -m - /dev/stdout | \
        samtools sort - | \
        samtools markdup -r -s - {output.bam} 2> {log}
        samtools index {output.bam}
        """

rule realignertargetcreator:
    input:
        bam = expand("bam/{sample}.MTonly_sorted_rmdup.bam", sample=SAMPLES),
        bai = expand("bam/{sample}.MTonly_sorted_rmdup.bam.bai", sample=SAMPLES)
    output:
        "bam/known.intervals"
    message: "Create interval lists using RealignerTargetCreator"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    params:
        gatk = f"{workflow.basedir}/resources/GenomeAnalysisTK.jar",
        reffa = f"{workflow.basedir}/resources/NC_012920.fa",
        dir = "bam",
        bams = " -I ".join([f"bam/{sample}.MTonly_sorted_rmdup.bam" for sample in SAMPLES]),
        knownindels = f"{workflow.basedir}/resources/known_indels.csv"
    shell:
        """
        java -Xms64m -Xmx16g -jar {params.gatk} -T RealignerTargetCreator \
             -R {params.reffa} \
             -o {params.dir}/sample.intervals \
             -I {params.bams}
        cat {params.dir}/sample.intervals \
            {params.knownindels} > {output}
        """

rule indel_realign:
    input:
        bam = "bam/{sample}.MTonly_sorted_rmdup.bam",
        bai = "bam/{sample}.MTonly_sorted_rmdup.bam.bai",
        intervals = "bam/known.intervals"
    output:
        "bam/{sample}.MTonly_indelrealign.bam"
    message: "Indel realignment: {wildcards.sample}"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    params:
        gatk = f"{workflow.basedir}/resources/GenomeAnalysisTK.jar",
        reffa = f"{workflow.basedir}/resources/NC_012920.fa",
        knownindels = f"{workflow.basedir}/resources/known_indels.csv"
    shell:
        """
        java -Xms64m -Xmx12g -jar {params.gatk} -T IndelRealigner \
             -targetIntervals {input.intervals} \
             -o {output} \
             -R {params.reffa} \
             -I {input.bam}
        """

rule mpileup:
    input:
        "bam/{sample}.MTonly_indelrealign.bam"
    output:
        temp("vcf/{sample}.gl.vcf")
    message: "Generate pileup: {wildcards.sample}"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    params:
        depth = 1000,
        fields = "DP,AD,ADF,ADR",
        reffa = f"{workflow.basedir}/resources/NC_012920.fa"
    shell:
        """
        bcftools mpileup -d {params.depth} \
                         -f {params.reffa} \
                         -a {params.fields} \
                         -O v -o {output} \
                         {input}
        """

rule call_gt:
    input:
        "vcf/{sample}.gl.vcf"
    output:
        "vcf/{sample}.vcf.gz"
    message: "Call genotypes: {wildcards.sample}"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    shell:
        """
        bcftools call \
            -o {output} \
            -A -O z --ploidy 1 -m \
            {input}
        """

rule consensus_fasta:
    # Generate FastA file and report about heteroplasmies and indels
    input:
        "vcf/{sample}.vcf.gz"
    output:
        fasta = "fasta/{sample}.fasta",
        het = "log/heteroplasmy/{sample}.heteroplasmy.txt",
        ind = "log/indels/{sample}.indels.txt"
    message: "Generate consensus FastA sequence for sample {wildcards.sample}"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    params:
        mtconsensus = f"{workflow.basedir}/resources/vcf.MTconsensus.py",
        indqual = 50
    shell:
        """
        {params.mtconsensus} \
                -i {input} \
                --hetreport {output.het} \
                --indreport {output.ind} \
                --indqual {params.indqual} \
                -o {output.fasta}
        """

rule haplogrep:
    input:
        "fasta/{sample}.fasta"
    output:
        "log/haplogrep/{sample}.hsd"
    message: "Haplogroup calling for sample {wildcards.sample}"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    params:
        haplogrep2 = f"{workflow.basedir}/resources/haplogrep-2.1.25.jar",
    shell:
        """
        java -Xms64m -Xmx3g -jar {params.haplogrep2} \
            --format fasta \
            --hits 5 \
            --extend-report \
            --in {input} \
            --out {output}
        """

rule sequencing_depth:
    # Generate coverage analysis for the whole MT genome per sample
    input:
        "bam/{sample}.MTonly_indelrealign.bam"
    output: 
        "log/depth/{sample}.depth.csv"
    message: "Generate coverage analysis for the whole MT genome: {wildcards.sample}"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    shell:
        """
        echo -e "chr\tpos\t{wildcards.sample}" > {output}
        samtools depth -a -r MT {input} >> {output}
        """

rule flagstat:
    # Determine the number of reads per sample
    input:
        "bam/{sample}.MTonly_indelrealign.bam"
    output:
        "log/flagstat/{sample}.flagstat"
    message: "Run flagstat analysis for {wildcards.sample}"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    shell:
        "samtools flagstat {input} > {output}"

rule subsampling:
    # Sub-sample the number of reads for the mixEMT analysis
    input:
        "log/flagstat/{sample}.flagstat"
    output:
        bam = temp("bam/{sample}.MTonly_indelrealign.subsampled.bam"),
        bai = temp("bam/{sample}.MTonly_indelrealign.subsampled.bam.bai")
    message: "Sub-sample reads of sample {wildcards.sample}"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    params:
        subsampling = lambda wildcards: True if float(mixemt_downsampling(f"logs/flagstat/{wildcards.sample}.flagstat")) < 1 else False,
        subsampling_fraction = lambda wildcards: mixemt_downsampling(f"logs/flagstat/{wildcards.sample}.flagstat"),
        bam = "bam/{sample}.MTonly_indelrealign.bam"
    shell:
        """
        if [[ -f {output.bam} ]]; then
            rm {output.bam}
        fi
        if [[ {params.subsampling} = "True" ]]; then  # subsampling
            samtools view \
                -bh \
                -s {params.subsampling_fraction} \
                -o {output.bam} \
                {input}
        else
            ln -s ${{PWD}}/{params.bam} {output.bam}
        fi
        samtools index {output.bam}
        """

rule mixemt:
    input:
        bam = "bam/{sample}.MTonly_indelrealign.subsampled.bam",
        bai = "bam/{sample}.MTonly_indelrealign.subsampled.bam.bai"
    output:
        log = "log/mixemt/{sample}.mixemt.log",
        pat = "log/mixemt/{sample}.mixemt.pos.tab"
    message: "Run mixEMT analysis for sample {wildcards.sample}"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    log: "log/mixemt/{sample}.mixemt.stderr"
    params: 
        mixemtprefix = "log/mixemt/{sample}.mixemt"
    shell:
        """
        python ${{CONDA_DEFAULT_ENV}}/bin/mixemt -v -t \
                {params.mixemtprefix} \
                {input.bam} \
                > {params.mixemtprefix}.log \
                2> {params.mixemtprefix}.stderr || \
        if [[ ($(wc -l < {params.mixemtprefix}.log) = "0") && ($(tail -1 {params.mixemtprefix}.stderr) = "0 contributors passed filtering steps.") ]]; then
            echo -e "hap1\tNA\tNA\t0" > {params.mixemtprefix}.log
            touch {params.mixemtprefix}.pos.tab
        fi
        """

rule summary:
    input:
        hsd = expand("log/haplogrep/{sample}.hsd", sample=SAMPLES),
        depth = expand("log/depth/{sample}.depth.csv", sample=SAMPLES),
        mixemt = expand("log/mixemt/{sample}.mixemt.log", sample=SAMPLES),
    output:
        "{projdir}/sample_summary.csv"
    message: "Prepare summary table"
    run:
        R("""
          library(data.table)
          library(tidyverse)
          
          # Number of reads
          flagstatfns <- list.files("log/flagstat", pattern = "\\\.flagstat", full.names = T)
          nreads <- map_df(flagstatfns, function(fn) {{
                          tibble(sample = str_replace(basename(fn), "\\\.flagstat", ""),
                                 `number of reads` = str_split(readLines(fn)[1], " ")[[1]][1])
                          }})
          # Duplicate frequency
          markduplicatesfns <- list.files("log/markduplicates", pattern = "\\\.markduplicates\\\.log", full.names = T)
          markduplicates <- map_df(markduplicatesfns, function(fn) {{
                              duplicated_reads <- readLines(fn) %>%
                                                  tail(1) %>%
                                                  str_split_fixed(., " ", n=3) %>%
                                                  .[1,3] %>% as.numeric()
                              tibble(sample = str_replace(basename(fn), "\\\.markduplicates\\\.log", ""),
                                     `number of duplicated reads` = duplicated_reads)
                            }})
          # Sequencing depth
          seqdepthfns <- list.files("log/depth", pattern = "\\\.depth\\\.csv", full.names = T)
          depth <- map_df(seqdepthfns, function(fn) {{
                          fread(fn, skip = 1, col.names = c("chr", "pos", "depth")) %>%
                          summarise(`mean depth` = round(mean(depth), 1),
                                    `sd depth` = round(sd(depth), 1),
                                    `min depth` = min(depth),
                                    `max depth` = max(depth),
                                    `sites with coverage >= 5-fold` = sum(depth > 4)) %>%
                          mutate(sample = str_replace(basename(fn), "\\\.depth\\\.csv", ""))
                          }}) %>%
                   select(sample, everything())
          # Haplogroup
          haplogrepfns <- list.files("log/haplogrep", pattern = "\\\.hsd", full.names = T)
          haplogrep <- map_df(haplogrepfns, function(fn) {{
                             fread(fn, nrows = 1, select = c("SampleID", "Haplogroup", "Quality")) %>%
                             rename(sample = SampleID,
                                    haplogroup = Haplogroup,
                                    `HG quality` = Quality)
                             }})
          # Heteroplasmies
          heteroplasmyfns <- list.files("log/heteroplasmy", pattern = "\\\.heteroplasmy\\\.txt", full.names = T)
          heteroplasmy <- map_df(heteroplasmyfns, function(fn) {{
                                tibble(sample = str_replace(basename(fn), "\\\.heteroplasmy\\\.txt", ""),
                                       `number of heteroplasmies` = fread(fn) %>% nrow(.))
                                }})
          # Indels
          indelsfns <- list.files("log/indels", pattern = "\\\.indels\\\.txt", full.names = T)
          indels <- map_df(indelsfns, function(fn) {{
                                tibble(sample = str_replace(basename(fn), "\\\.indels\\\.txt", ""),
                                       `number of indels` = fread(fn) %>% 
                                                            filter(idv >= 3, imf >= 0.5) %>%
                                                            nrow(.))
                                }})
          # MixEMT
          mixemtfns <- list.files("log/mixemt", pattern = "\\\.mixemt\\\.log", full.names = T)
          mixemt <- map_df(mixemtfns, function(fn) {{
                           fread(fn, header = F,
                                 col.names = c("comp", "hg", "contr", "noReads")) %>%
                           mutate(sample = str_replace(basename(fn), "\\\.mixemt\\\.log", ""),
                                  contr = paste0(contr * 100, "%"),
                                  label = paste0(hg, " (", contr, ",", noReads, ")")) %>%
                           group_by(sample) %>%
                           summarise(mixEMT = str_replace_all(toString(label), ", ", "; "))
                           }})
          
          # Summarise
          sample_summary <- haplogrep %>%
                            left_join(mixemt, by = "sample") %>%
                            left_join(nreads, by = "sample") %>%
                            left_join(markduplicates, by = "sample") %>%
                            left_join(depth, by = "sample") %>%
                            left_join(heteroplasmy, by = "sample") %>%
                            left_join(indels, by = "sample")
          fwrite(sample_summary, sep = "\t", file = "{output}")
        """)

rule concatenate_fastas:
    input:
        expand("log/haplogrep/{sample}.hsd", sample=SAMPLES),
    output:
        "{projdir}/sequences.fasta"
    message: "Concatenate the FastA files"
    params:
        dir = "fasta"
    shell:
        """
        cat {params.dir}/*.fasta > {output}
        """

rule copy_to_projdir:
    input:
        hsd = expand("log/haplogrep/{sample}.hsd", sample=SAMPLES),
        depth = expand("log/depth/{sample}.depth.csv", sample=SAMPLES),
        mixemt = expand("log/mixemt/{sample}.mixemt.log", sample=SAMPLES),
    output:
        "{projdir}/{sample}/{sample}.vcf.gz"
    message: "Generate per-sample folder with all information: {wildcards.sample}"
    conda: f"{workflow.basedir}/envs/conda.yaml"
    params:
        dir = "{projdir}/{sample}"
    shell:
        """
        cp bam/{wildcards.sample}.MTonly_indelrealign.bam {params.dir}/{wildcards.sample}.MT.bam
        cp bam/{wildcards.sample}.MTonly_indelrealign.bai {params.dir}/{wildcards.sample}.MT.bai
        cp fasta/{wildcards.sample}.fasta {params.dir}/
        cp log/depth/{wildcards.sample}.depth.csv {params.dir}/
        cp log/haplogrep/{wildcards.sample}.hsd {params.dir}/
        cp log/heteroplasmy/{wildcards.sample}.heteroplasmy.txt {params.dir}/
        cp log/indels/{wildcards.sample}.indels.txt {params.dir}/
        cp log/markduplicates/{wildcards.sample}.markduplicates.log {params.dir}/
        cp -r log/mixemt/{wildcards.sample}.mixemt.{{log,stderr}} {params.dir}/
        cp vcf/{wildcards.sample}.vcf.gz {params.dir}/
        """

rule remove_temp:
    params:
        tmpdir = config['tmpdir']
    shell:
        "rm -rf {params.tmpdir}"
