#!/usr/bin/env python
########################################################################
# Evaluates a VCF file with MT genotypes and returns a consensus
# sequence in FastA format and some summary statistics
#
# Alex Huebner, 08/07/16
########################################################################

from __future__ import division, print_function
import argparse, os, sys
import pysam
import numpy as np
import pandas as pd
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

iupac_ambiguous = {
    'AC': 'M',
    'AG': 'R',
    'AT': 'W',
    'CG': 'S',
    'CT': 'Y',
    'GT': 'K',
    'ACG': 'V',
    'ACT': 'H',
    'AGT': 'D',
    'CGT': 'B',
}


def main():
    ''' Parses a VCF file with MT genotypes and takes the following
        measures:

        - reference sites are just wrote to FastA output
        - variable sites are evaluated for the MAF:
            1) the majority allele is written to FastA output
            2) the MAF is reported to a heteroplasmy report file
            3) if enable: if the ratio between the major and the minor
            allele is between 0.3 and 0.7, an ambigious base is written to
            FastA output
        - indels: indels are reported to indel file report but only
          written to FastA output if the indel is supported by more than a
          specified threshold of reads

        Parsing the VCF file with indels is a bit complicated. If there is an
        indel, the site with the indel is duplicated with the non-indel line
        before the indel line. Therefore, the last site is kept in memory (see
        variable starting with lastsite) and only after checking if the next
        line is not an indel it is written to file.
    '''
    vcffile = pysam.VariantFile(Args['input'])

    # Prepare FastA file for output
    consensusfile = open(Args['output'], "wt")

    # Prepare variables
    consensussequence = []
    heteroplasmies = []
    indels = []
    lastsite = ""
    lastsite_pos = 0
    lastsite_idx = []
    indellength = 0
    if Args['amb']:
        try:
            threshold = Args['het']
        except KeyError:
            print("Argument 'het' was not provided but is required when using" +
                  "option 'amb'.", file=sys.stderr)
            sys.exit(1)
    else:
        threshold = 1

    # Parse VCF file
    for site in vcffile.fetch():
        # Extract for each site the indices of the FORMAT fields allele counts
        try:
            ac_idx = [site.format.keys().index(FF) for FF in ['AD', 'ADF', 'ADR']]
        except IndexError:
            print("The VCF file doesn't contain the necessary FORMAT fields AD, " +
                  "ADF, and/or ADR. Provide VCF file with these fields.")
        # Check if site is INDEL: INDEL sites are duplicated with the non-Indel
        # line being first, the indel line second; both have the same position
        if site.pos != lastsite_pos:  # no INDEL
            if indellength == 0:  # not overlapping with an indel
                if lastsite_pos != 0:  # not the first position
                    consensusbase = parse_consensus(lastsite, lastsite_idx[0],
                                                    Args['amb'], threshold)
                    consensussequence.append(consensusbase)
                    hetinfo = parse_heteroplasmies(lastsite, lastsite_idx)
                    if hetinfo is not None:
                        heteroplasmies.append(hetinfo)
            else:
                indellength -= 1
            lastsite = site
            lastsite_pos = site.pos
            lastsite_idx = ac_idx
        else:  # INDEL
            if indellength == 0:
                if site.info.values()[2] >= Args['ind'] or site.qual >= Args['indqual']:
                    consensusbase = parse_consensus(site, ac_idx[0],
                                                    Args['amb'], threshold)
                    consensussequence.append(consensusbase)
                    if site.alts is None:
                        indellength = len(site.alleles[0]) - 1
                    else:
                        if len(site.alleles[0]) > len(site.alleles[1]) and site.pos != 3106:
                            indellength = len(site.alleles[0])
                        else:
                            indellength = len(site.alleles[1]) - 1
                else:
                    consensusbase = parse_consensus(lastsite, lastsite_idx[0],
                                                    Args['amb'], threshold)
                    consensussequence.append(consensusbase)
                    indellength = 1
                if site.alts is None:
                    indels.append([site.pos, site.ref, site.alleles[0], site.info.values()[1],
                                   site.info.values()[2]])
                else:
                    indels.append([site.pos, site.ref, site.alleles[1], site.info.values()[1],
                                   site.info.values()[2]])
                hetinfo = parse_heteroplasmies(lastsite, lastsite_idx)
                if hetinfo is not None:
                    heteroplasmies.append(hetinfo)

    # Write last entry of VCF file
    if indellength == 0:
        consensusbase = parse_consensus(lastsite, lastsite_idx[0],
                                        Args['amb'], threshold)
        consensussequence.append(consensusbase)
        hetinfo = parse_heteroplasmies(site, lastsite_idx)

    # Heteroplasmy report
    heteroplasmy_report = pd.DataFrame(heteroplasmies,
                                       columns=['position', 'ref', 'majorAllele',
                                                'majorAlleleFreq', 'minorAllele',
                                                'minorAlleleFreq'])
    heteroplasmy_report.to_csv(Args['hetreport'], sep="\t", float_format="%.4f", index=False)

    # Indel report
    indel_report = pd.DataFrame(indels, columns=['position', 'ref', 'indel', 'idv', 'imf'])
    indel_report.to_csv(Args['indreport'], sep="\t", float_format="%.4f", index=False)

    # Consensus sequence
    consensussequence = "".join(consensussequence)
    sampleid = os.path.basename(Args['input']).split(".")[0]
    SeqIO.write(SeqRecord(Seq(consensussequence), id=sampleid, description=""),
                consensusfile, "fasta")
    consensusfile.close()


def parse_consensus(site, idx, amb, threshold):
    ''' Parse the site regarding its consensus sequence

        Parameters:
        site      : complete site as pysam.cbcf.VariantRecord
        idx       : index of the FORMAT fields AD (allele counts per allele)
        amb       : Args value of 'amb'
        threshold : Args value of 'het'

        Returns:
        consensus allele as string
    '''
    if site.samples[0].values()[0][0] is None:  # non-typed
        return 'N'
    elif site.alts is None:  # No variant position
        return site.alleles[0]
    else:  # Variant position
        afs = allelefrequencies(site.samples[0].values()[idx])
        # Check whether MAF is > threshold to report ambiguity code
        if amb:
            if np.partition(afs, 1)[-2] > threshold:
                return iupac_ambiguous["".join(sorted([site.alleles[i]
                                               for i in np.argsort(afs)[-2:]]))]
        else:  # return major allele
            return site.alleles[np.argmax(afs)]


def parse_heteroplasmies(site, idcs):
    ''' Parse a site regarding heteroplasmies: reported heteroplasmies fulfill
        the following criteria:
        - 2 allele counts per strand for minor allele
        - minor allele frequency per strand is > 2%

        Parameters:
        site : complete site as pysam.cbcf.VariantRecord
        idcs : indices of the FORMAT fields AD (allele counts per allele),
               ADF (allele counts per allele on forward strand),
               ADR (allele counts per allele on reverse strand)

        Returns:
        If the two criteria are not fulfill:
            None
        Else:
            list of position, major and minor allele and their frequencies
    '''
    if site.alts is not None:
        acs = [site.samples[0].values()[idx]
               for idx in idcs[1:]]
        acs = np.vstack(acs)
        acs = np.sort(acs)
        if (acs[:, -2] > 1).sum() == 2:  # 2 AC per strand
            afs = np.apply_along_axis(allelefrequencies, 1, acs)
            afs = np.sort(afs)
            if (afs[:, -2] >= 0.02).sum() == 2:  # MAF > 2% on both strands
                alleles = [site.alleles[i] for i in np.argsort(site.samples[0].values()[idcs[0]])]
                afs = np.sort(allelefrequencies(site.samples[0].values()[idcs[0]]))
                return [site.pos, site.ref, alleles[-1], afs[-1], alleles[-2], afs[-2]]
        else:
            return None
    else:
        return None


def allelefrequencies(ac):
    ''' Calculates the major and minor allele frequencies based on NumPy
        array of allele counts
    '''
    ac = np.asarray(ac)
    return ac / ac.sum()


########################################################################
# Argument parser
########################################################################
Parser = argparse.ArgumentParser(description='Concensus calling and ' +
                                 'heteroplasmy and indel support reports ' +
                                 'for MT genotypes')
Parser.add_argument('-i', '--input', help='input VCF file with MT ' +
                    'genotypes', required=True)
Parser.add_argument('--amb', action='store_true', help='write ambigious base ' +
                    'when the MAF is > threshold')
Parser.add_argument('--het', type=float, default=0.3, help='minimum MAF for ' +
                    'for writing ambigious base from major and minor allele; ' +
                    'only works with option --amb [0.3]')
Parser.add_argument('--hetreport', help='Report with MAF frequency for ' +
                    'heteroplasmic sites [MAF > 0.005]', required=True)
Parser.add_argument('--ind', type=float, default=0.5, help='minimum fraction ' +
                    'of reads supporting indel for writing indel into ' +
                    'consensus sequence [0.5]')
Parser.add_argument('--indqual', type=int, default=0, help='minimum QUAL for ' +
                    'for writing indel into consensus sequence [0]')
Parser.add_argument('--indreport', help='report about INDELs', required=True)
Parser.add_argument('-o', '--output', help='consensus sequence in FastA format')
Args = vars(Parser.parse_args())

########################################################################
# Routine
########################################################################
if __name__ == '__main__':
    main()
